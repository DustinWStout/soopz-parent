var wordCounter = {
    el: '.word-count',

    initialize: function (new_class) {
        var _this = this;

        this.el = new_class;

        jQuery('.editor_page').parent(".row").find('textarea').on('change', function () {
            _this.update(jQuery(this).val(), jQuery(this).parent());
        });
        jQuery('.editor_page').parent(".row").find('textarea').on('keyup', function () {
            _this.update(jQuery(this).val(), jQuery(this).parent());
        });
    },

    start:function(){
        jQuery('.all_soopz_inputs').find('.i_post_editor_input').each(function(){
            var value = jQuery(this).val();
            var parent = jQuery(this).parent();
            jQuery('.chars', parent).html(wordCounter.getCharCount(value));
            jQuery('.words', parent).html(wordCounter.getWordCount(value.trim()));
            jQuery('.paras', parent).html(wordCounter.getParaCount(value.trim()));
        });
    },


    update: function (value, parent) {
        // console.log(this.el);
        jQuery('.chars', parent).html(this.getCharCount(value));
        jQuery('.words', parent).html(this.getWordCount(value.trim()));
        jQuery('.paras', parent).html(this.getParaCount(value.trim()));
//                global-count
        var char = 0;
        var word = 0;
        var paras = 0;
        var this_class = this;
        jQuery('.editor_page').parent(".row").find('textarea').each(function () {
            char = char + this_class.getCharCount(jQuery(this).val());
            word = word + this_class.getWordCount(jQuery(this).val());
            paras = paras + this_class.getParaCount(jQuery(this).val());
        })
        jQuery("#global-count .chars").html(char);
        jQuery("#global-count .words").html(word);
        jQuery("#global-count .paras").html(paras);
    },

    /*
     * getCharCount:
     *   - Calculates the number of characters in the field.
     *   - Counts *all* characters.
     */

    getCharCount: function (value) {
        return value.length;
    },

    /*
     * getWordCount:
     *   - Calculates the number of words in the field.
     *   - Words are separated by any number of spaces or a semi-colon.
     */

    getWordCount: function (value) {
        if (value) {
            var regex = /\s+|\s*;+\s*/g;
            return value.split(regex).length;
        }

        return 0;
    },

    /*
     * getParaCount:
     *   - Calculates the number of paragraphs in the field.
     *   - Paragraphs are separated by any number of newline characters.
     */

    getParaCount: function (value) {
        if (value) {
            var regex = /\n+/g;
            return value.split(regex).length;
        }

        return 0;
    }
};

wordCounter.initialize(".word-count");


jQuery(document).ready(function ($) {
    $('textarea.form-control').keyup(function () {
        var keyed = $(this).val();
        $("ul.counters").removeClass("text_is_typed");
        if (keyed != "") {
            $("ul.counters").addClass("text_is_typed");
        } else {
            $("ul.counters").removeClass("text_is_typed");

        }
    });
    var change_class_number = 1;
    $(".new_textarea2").click(function () {
        //  $( '<textarea onkeyup="textAreaAdjust(this)" name="content" class="new_textarea_fild form-control" type="text" placeholder="Type your social message..."></textarea>' ).insertBefore(this);
        var new_txt_html = '<div class="flex-wrapper soopz-input i_post_input_item_div word-count_' + change_class_number + '">';
        new_txt_html += '<textarea  rows="1" name="content" class="autoExpand flex-item form-control i_post_content_item i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>';
//        new_txt_html += '<textarea  rows="1" data-min-rows="1" name="content" class="autoExpand flex-item form-control i_post_content_item i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>';
//        new_txt_html += '<textarea onkeyup="textAreaAdjust(this)" name="content" class="flex-item form-control i_post_content_item i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>';
//        new_txt_html += '<textarea oninput="textAreaAdjust(this)" name="content" class="flex-item form-control i_post_content_item i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>';
        new_txt_html += '<div class="flex-item counters"><ul class="counters"><li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li><li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>';
        new_txt_html += '<li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>';
        new_txt_html += '<li> <i class="ion ion-minus-circled i_remove_filed" aria-hidden="true" title="Remove this field"></i> </li>';
        new_txt_html += '</ul> </div></div>';
        var new_text_area = $(new_txt_html);
        new_text_area.insertBefore(this);
        setBaseScrollHeight(new_text_area.find("textarea")[0]);
        wordCounter.initialize(".word-count_" + change_class_number);
        change_class_number++;
    });




    $('#btn-open').click(function () {
        var val_text = "";
        $(".row .flex-wrapper.soopz-input").each(function (key, value) {
            var val_text_ = $(this).find("textarea").val();
            val_text = val_text + "<p>" + val_text_ + "</p>";
        });
        $("#popup-content .popup_content").html(val_text);
        $.basicpopup({
            content: $('#popup-content').html()
        });

    });
//$('.button.popup-button').click(function(){
    $(document).on('click', ".button.popup-button", function () {
        $.basicpopup({
            content: $('#popup-content-share').html()
        });

    });


    /*$('#i_loading').hide();
    $(document).on('click', '#btn-save', function () {
        $('#i_loading').show();
        var textarea_val = "";
        var counter = 0;
        var post_title = "";
        $(".row .flex-wrapper.soopz-input").each(function (key, value) {
            var textarea_val_ = $(this).find("textarea").val();
            if (counter == 0) {
                post_title = textarea_val_;
            }
            if (counter != 0) {
                textarea_val = textarea_val + "<p>" + textarea_val_ + "</p>";
            }
            counter++;
        });
        ajax_request = jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'save_post',
                textarea_val: textarea_val,
                post_title: post_title
            },
            success: function (response) {
                response = jQuery.parseJSON(response);
                console.log(response);
                if (response) {
                    $('#i_loading').hide();
                    $(".post_success_message").css("display", "inline-block");
                    setTimeout(function () {
                        $(".post_success_message").css("display", "none");
                    }, 4000);
                }
            }
        });
    });*/

    $('#i_loading_for_popup').hide();
    $(document).on('click', '#popup-save-btn', function () {
        $('#i_loading_for_popup').show();
        var textarea_val = "";
        var counter = 0;
        var post_title = "";
        $(".row .flex-wrapper.soopz-input").each(function (key, value) {
            var textarea_val_ = $(this).find("textarea").val();
            if (counter == 0) {
                post_title = textarea_val_;
            }
            if (counter != 0) {
                textarea_val = textarea_val + "<p>" + textarea_val_ + "</p>";
            }
            counter++;
        });
        ajax_request = jQuery.ajax({
            url: ajaxurl,
            type: 'post',
            data: {
                action: 'save_post',
                textarea_val: textarea_val,
                post_title: post_title
            },
            success: function (response) {
                response = jQuery.parseJSON(response);
                console.log(response);
                if (response) {
                    $('#i_loading_for_popup').hide();
                    $(".post_success_message_for_popup").css("display", "inline-block");
                    setTimeout(function () {
                        $(".post_success_message_for_popup").css("display", "none");
                    }, 4000);
                }
            }
        });

    });


});
