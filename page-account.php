<?php
/*
Template Name: account
*/

if ( !is_user_logged_in() ){
    wp_redirect( home_url() );
}
if($_GET['delete_acc'] == 'yes'){
	$user = wp_get_current_user();
	require_once(ABSPATH.'wp-admin/includes/user.php' );
	wp_delete_user($user->ID);
	wp_redirect(home_url());
	exit;
}
get_header();
$site_url = get_site_url();

?>
<div class="twelve columns word-count account_page">
    <div class="user_info">
        <h4><?php _e('Your info:'); ?></h4>
        <div class="user_all_data">
            <?php
            $user_info = get_user_meta( get_current_user_id() );
            $user = wp_get_current_user();
            $user_name = $user_info["first_name"][0];
            $user_last_name = $user_info["last_name"][0];
            $user_id = $user->ID;
            $user_email = $user->user_email;
            ?>
            <form id="update_user_data" class="i_relative" action="#">
                <input class="user_data user_name" type="text" id="user_firstname" name="firstname" value="<?php echo $user_name;?>" placeholder="First name">
                <input class="user_data user_last_name" type="text" id="user_lastname" name="lastname" value="<?php echo $user_last_name;?>" placeholder="Last name" value="">
                <input class="user_data user_email" type="text" id="user_email" name="email" value="<?php echo $user_email;?>" placeholder="Email">
                <input class="user_data user_pass" type="password"id="user_pass" name="pass" value="" placeholder="Password">
                <div class="i_relative">
                    <input class="user_data form_submit" type="submit" id="save_user_data" value="Save">
                    <div id="i_update_user_data_msg" class="i_ajax_msg"></div>
                </div>
            </form>
        </div>
    </div>
    <div class="connected_accounts">
        <h4>Connected Account(s):</h4>
        <div class="all_connection all_social_buttons">
            <?php
            if ( new_google_is_user_connected() ) {
                ?>
                <div class='google_plus_button'>
                    <span class="button google-plus-button cursor_default" href="#">Google+</span>
                </div>
                <?php
            }
            ?>
            <?php
            if ( new_fb_is_user_connected() ) {
                ?>
                <div class='facebook_button'>
                    <span class="button facebook-button cursor_default" href="#">Facebook</span>
                </div>
            <?php
            }
            ?>
            <?php
            if ( new_twitter_is_user_connected() ) {
                ?>
                <div class='twitter_button'>
                    <span class="button twitter-button cursor_default" href="#">Twitter</span>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
    <div class="connect_other_account">
        <h4><?php _e('Connect Another Account:'); ?></h4>
        <div class="conect_with_other_social">
            <span class="ion-ios-plus-outline"></span>
            <div class="all_social_buttons">
            <?php
            if ( !new_google_is_user_connected() ) {
                ?>
                <div class='google_plus_button'>
                    <a href="<?php echo $site_url;?>/wp-login.php?loginGoogle=1&redirect=<?php echo $site_url;?>"
                       class="button google-plus-button"
                       onclick="window.location = <?php echo $site_url;?>'/wp-login.php?loginGoogle=1&redirect='+window.location.href; return false;">Google+</a>
                </div>
                <?php
            }
            ?>

            <?php
            if ( !new_fb_is_user_connected() ) {
            ?>
                <div class='facebook_button'>
                    <a href="<?php echo $site_url;?>/wp-login.php?loginFacebook=1&redirect=<?php echo $site_url;?>"
                       class="button facebook-button"
                       onclick="window.location = <?php echo $site_url;?>'/wp-login.php?loginFacebook=1&redirect='+window.location.href; return false;">Facebook</a>
                </div>
                <?php
            }
            ?>

            <?php
            if ( !new_twitter_is_user_connected() ) {
                ?>
                <div class='twitter_button'>
                    <a href="<?php echo $site_url;?>/wp-login.php?loginTwitter=1&redirect=<?php echo $site_url;?>"
                       class="button twitter-button"
                       onclick="window.location = <?php echo $site_url;?>'/wp-login.php?loginTwitter=1&redirect='+window.location.href; return false;">Twitter</a>
                </div>
                <?php
            }
            ?>
                <div class='buffer_button'>
                    <a href=""
                       class="button buffer-button">Buffer</a>
                </div>

            </div>
        </div>
    </div>
    <div class="sharing_options">
        <h4>Sharing Options:</h4>
        <div id="social-buttons" class="ten columns">
            <?php
            if (function_exists('social_warfare')):
                social_warfare();
            endif;
            ?>
        </div>
    </div>
    <div class="usage">
        <h4>Usage</h4>
        <div class="user_storage">
            <span class="storage_size"><i class="ion-happy" style="margin-right: 5px;"></i> of <i class="ion-ios-infinite" style="margin: 5px;"></i> posts</span>
            <span class="used_place"></span>
            <span class="free_place"></span>
        </div>
        <div class="upgrate_storage">
            <span class="button">Upgrate Storage</span>
        </div>
        <a class="delete_acc" href="?delete_acc=yes">Delete account (cannot be undone)</a>
    </div>

</div>
<?php get_footer(); ?>
