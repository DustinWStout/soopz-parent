<?php
global $i_user_info;
$theme_url=get_template_directory_uri().'/';

$template_directory = get_template_directory();

if ( !class_exists('ThemeUpdateChecker') ) {
    require($template_directory . '/inc/theme-update-checker/theme-update-checker.php');
}
$example_update_checker = new ThemeUpdateChecker(
    'soopz',
    'https://bitbucket.org/wisersites/soopz-parent-updates/raw/master/info.json'
);

// Theme support
add_theme_support( 'title-tag' ); 
add_theme_support('post-thumbnails');


// Admin bar
if(!current_user_can('administrator')){
    show_admin_bar( false );
}


// Option page
if(function_exists('acf_add_options_page')){
    acf_add_options_page( array(
        'page_title'=> 'Theme Options',
        'menu_title' => 'Theme Options',
        'menu_slug' => 'soopz_options',
    ) );
}




// Register Post Types - Taxonomies
function soopz_register_post_types_taxonomies() {
//    POST TYPES
    $labels = array(
        'name'                  => 'Questions',
        'singular_name'         => 'Question',
        'add_new_item'          => 'Add new Question',
        'new_item'              => 'New Question',
        'edit_item'             => 'Edit Question',
        'view_item'             => 'View Question',
        'all_items'             => 'All Questions',
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-editor-help',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'question' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => 5,
        'supports'           => array('title','editor'),
    );
    register_post_type( 'question', $args );
    

    $labels = array(
        'name'                  => 'Tips',
        'singular_name'         => 'Tip',
        'add_new_item'          => 'Add new Tip',
        'new_item'              => 'New Tip',
        'edit_item'             => 'Edit Tip',
        'view_item'             => 'View Tip',
        'all_items'             => 'All Tips',
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-warning',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'tip' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => true,
        'menu_position'      => 5,
        'supports'           => array('title','editor'),
    );
    register_post_type( 'tip', $args );


//================================================================================================
    
//    Taxonomies
    $labels = array(
        'name' => 'Networks',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'question-category' ),
    );
   register_taxonomy( 'question-category', array( 'question' ), $args );
    

    $labels = array(
        'name' => 'Networks',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'tip-category' ),
    );
   register_taxonomy( 'tip-category', array( 'tip' ), $args );

}


add_action( 'init', 'soopz_register_post_types_taxonomies' );


// Widgets
function footer_widgets_init() {
	register_sidebar( array(
		'name' => __( 'Footer Sidebar', 'footer_sidebar' ),
		'id' => 'sidebar_footer',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );
	register_sidebar( array(
		'name' => __( 'Footer Photos', 'footer_sidebar' ),
		'id' => 'sidebar_photos',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget' => "</aside>",
		'before_title' => '<div class="widget-title">',
		'after_title' => '</div>',
	) );

}
add_action( 'widgets_init', 'footer_widgets_init' );

//GX
//Register nav menus
if (function_exists('register_nav_menus')) {
    register_nav_menus(
        array(
            'main_menu'=>'Main Navigation Menu',
            'header_menu'=>'Header Navigation Menu',
        )
    );
}
add_filter( 'wp_nav_menu_items', 'wti_loginout_menu_link', 10, 2 );
function wti_loginout_menu_link( $items, $args ) {
    global $login_url;
    if ( $args->theme_location == 'main_menu' || $args->theme_location == 'foot_menu' ) {
        if (is_user_logged_in()) {
            $items .= '<li class="menu-item"><a href="'. wp_logout_url( home_url() ) .'">Sign Out</a></li>';
        } else {
            //$items .= '<li class="menu-item i_login_menu"><a href="'. $login_url .'">Sign In</a></li>';
        }
    }
    return $items;
}

function soopz_core_init(){
    global $i_user_info;
    $i_user_info = array();
    if ( is_user_logged_in() ) {
        $current_user = wp_get_current_user();
        $user_metas = get_user_meta( get_current_user_id() );

        $user_id = $current_user->ID;
        $profile_picture = get_avatar_url( $current_user->user_email, array(
            'size' => 48,
            'default'=>'identicon',
        ));

        //Get Profile Image from social metas
        if( $fb_profile_picture = get_user_meta(get_current_user_id(), 'fb_profile_picture', true) ){
            $profile_picture = $fb_profile_picture;
        } elseif( $twitter_profile_picture = get_user_meta(get_current_user_id(), 'twitter_profile_picture', true) ){
            $profile_picture = $twitter_profile_picture;
        } elseif( $google_profile_picture = get_user_meta(get_current_user_id(), 'google_profile_picture', true) ){
            $profile_picture = $google_profile_picture;
        }

        $i_user_info['user_id'] = $user_id;
        $i_user_info['profile_picture'] = $profile_picture;
        $i_user_info['user_name'] = $user_metas["first_name"][0];
        $i_user_info['last_name'] = $user_metas["last_name"][0];
        $i_user_info['display_name'] = $current_user->display_name;
        if( $i_user_info['user_name'] || $i_user_info['last_name'] ){
            $i_user_info['display_name'] = $i_user_info['user_name'] . ' ' . $i_user_info['last_name'];
        }
    }
}
add_action( 'init', 'soopz_core_init' );



// Define wp-admin ajax url
add_action('wp_head','theme_ajaxurl');
function theme_ajaxurl() {
?>
 <script type="text/javascript">
 var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
 </script>
<?php
}

// Register scripts and styles
function soopz_scripts() {
    global $theme_url;
    $theme_dir = get_stylesheet_directory_uri();

    // Scripts
    wp_enqueue_script( 'basicPopup', $theme_dir.'/lib/basicPopup/dist/js/jquery.basicPopup.js', array ('jquery'), '1.0.0', false );
    wp_enqueue_script( 'autosize', $theme_dir.'/lib/autosize/autosize.js', array ('jquery'), '1.0.0', false );


    wp_enqueue_script( 'word-counter', $theme_dir.'/word-counter.js', array ('jquery'), '1.0.0', true );
    wp_enqueue_script( 'jquery-confirm', $theme_dir.'/lib/jquery-confirm/jquery-confirm.js', array ('jquery'), '1.0.0', false );
    wp_enqueue_script( 'nice-select', $theme_dir.'/lib/nice-select/jquery.nice-select.min.js', array ('jquery'), '1.0.0', false );

    wp_enqueue_script( 'scriptJS', $theme_dir.'/script.js', array ('jquery'), '1.0.0', false );

    // Localize Scripts
    wp_localize_script( 'scriptJS', 'i_infos',
        array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'redirecturl' => home_url(),
            'loadingmessage' => __('Sending user info, please wait...'),
            'loading_img' => '<img src="'.IEVO_PLUGIN_URL.'images/loading.gif" id="i_loading_img">',
            'site_url' => site_url(),
            'theme_url' => $theme_url,
            'post_edit_permalink' => get_permalink(55)
        )
    );

    // Styles
    wp_enqueue_style( 'normalize', $theme_dir . '/css/normalize.css', array(), 1.0, 'all' );
    wp_enqueue_style( 'skeleton', $theme_dir . '/css/skeleton.css', array(), 1.0, 'all' );
     wp_enqueue_style( 'nice-select', $theme_dir . '/lib/nice-select/nice-select.css', array(), 1.0, 'all' );
    // wp_enqueue_style( 'font-awesome', $theme_dir . '/lib/font-awesome/css/font-awesome.css', array(), 1.0, 'all' );
    wp_enqueue_style( 'ionicons', $theme_dir . '/lib/ionicons/css/ionicons.min.css', array(), 1.0, 'all' );
    wp_enqueue_style( 'basicPopup', $theme_dir . '/lib/basicPopup/dist/css/basicPopup.css', array(), 1.0, 'all' );
    wp_enqueue_style( 'basicPopupDefault', $theme_dir . '/lib/basicPopup/dist/css/basicPopupDefault.css', array(), 1.0, 'all' );
    wp_enqueue_style( 'jquery-confirm', $theme_dir . '/lib/jquery-confirm/jquery-confirm.css', array(), 1.0, 'all' );

    wp_enqueue_style( 'style', $theme_dir . '/style.css', array(), 1.0, 'all' );

}
add_action( 'wp_enqueue_scripts', 'soopz_scripts' );


/*
* Ajax Requesting
*/

// Ajax Login
function i_ajax_login(){

    // First check the nonce, if it fails the function will break
    //check_ajax_referer( 'ajax-login-nonce', 'security' );
    if ( ! ( isset( $_REQUEST['action'] ) && 'i_ajaxlogin' == $_POST['action'] )  )
        return;

    // Nonce is checked, get the POST data and sign user on
    $info = array();
    $info['user_login'] = $_POST['username'];
    $info['user_password'] = $_POST['password'];
    if( $_POST['rememberme'] == 1 ){
        $info['remember'] = true;
    } else {
        $info['remember'] = false;
    }

    $user_signon = wp_signon( $info, false );
    if ( is_wp_error($user_signon) ){
        echo json_encode(array('loggedin'=>false, 'message'=>__('Wrong username or password.')));
    } else {
        echo json_encode(array('loggedin'=>true, 'message'=>__('Login successful, redirecting...')));
    }

    exit;
}
add_action( 'wp_ajax_nopriv_i_ajaxlogin', 'i_ajax_login' );


// Save posts
function soopz_save_post() {
    if ( ! ( isset( $_REQUEST['action'] ) && 'soopz_save_post' == $_POST['action'] )  )
        return;

    $return = array(
        'error' => '1',
        'html' => 'Bad value'
    );

    if ( !is_user_logged_in() )
        exit( json_encode( $return ) );


    $post_images = $_FILES['post_images'];
    $verify_images = $_POST['verify_images'];
    $user_id = get_current_user_id();
    $post_contents = $_POST['post_contents'];
    $social_medias = $_POST['social_medias'];
    $scores = $_POST['scores'];

    $names = $post_images['name'];

    $tmp_name = $post_images['tmp_name'];
    $type = $post_images['type'];
    $new_post = array(
        'post_title'    => $post_contents[0],
        'post_author' => $user_id,
        'post_status'   => 'publish'
    );

        // Insert the post into the database
        $post_id = wp_insert_post( $new_post );
        $i=-1;
        $k=-1;
        foreach($post_contents as $content){ $i++;

            if($verify_images[$i] == 'image'){ $k++;
                $filename = $names[$k];
                $wp_upload_dir = wp_upload_dir();
                move_uploaded_file( $tmp_name[$k], $wp_upload_dir['path']  . '/' . $filename );
                $attachment = array(
                    'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ), 
                    'post_mime_type' => $type[$k],
                    'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                    'post_content' => '',
                    'post_status' => 'inherit'
                );
                $filename = $wp_upload_dir['path']  . '/' . $filename;
                $attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                wp_update_attachment_metadata( $attach_id, $attach_data );  
            }else{
                $attach_id = null;
            }

            $final[] = array(
                'type'=>$social_medias[$i],
                'image'=>$attach_id,
                'content'=>$content,
                'score'=>$scores[$i],
            );
        }

        update_field('field_599256824ec27',$final,$post_id);

        
        if( $post_id ){
            $return = array(
                'error' => '0',
                'post_title' => $post_contents[0],
                'post_id' => $post_id,
                'html' => 'Your new post has been successfully saved!',
            );
        } else {
            $return = array(
                'error' => '1',
                'post_title' => $post_contents[0],
                'html' => 'Cant Add New Post!',
            );
        }

    echo json_encode( $return );
    exit;
}
add_action( 'wp_ajax_soopz_save_post', 'soopz_save_post' );


// Edit Post
function soopz_edit_post( ){
    if ( ! ( isset( $_REQUEST['action'] ) && 'soopz_edit_post' == $_POST['action'] )  )
        return;

    $return = array(
        'error' => '1',
        'html' => 'Bad value'
    );

    if ( !is_user_logged_in() )
        exit( json_encode( $return ) );

    $post_id = $_POST['post_id'];
    $user_id = get_current_user_id();

    $posts = get_field('posts',$post_id);
    $output = "";
    if(!empty($posts) && is_array($posts)){
       $i=-1;
        foreach($posts as $post){ $i++;
            if($post['score'] == ''){
                $score = 0;
            }else{
                $score = $post['score'];
            }
            $output .= '<div class="flex-wrapper soopz-input">';

            if($post['type'] != ''){
                $output .= '<span class="select_social_type" style="visibility: visible;"><i class="sw sw-'.$post['type'].' social_'.$post['type'].' social_active_type white_icon" social_media="'.$post['type'].'"></i></span>';
            }else{
                $output .= '<span class="select_social_type" style="visibility: visible;"><i class="ion-android-person social_active_type social_profile"></i></span>';
            }
            
            $output .= '
            <div class="social_types">
                <a href="" title="Facebook"><i class="sw sw-facebook social_facebook"></i></a>
                <a href="" title="Linkedin"><i class="sw sw-linkedin social_linkedin"></i></a>
                <a href="" title="Twitter"><i class="sw sw-twitter social_twitter"></i></a>
                <a href="" title="Instagram"><i class="sw sw-instagram social_instagram"></i></a>
                <a href="" title="Google+"><i class="sw sw-google-plus social_google-plus"></i></a>
                <a href="" title="Pinterest"><i class="sw sw-pinterest social_pinterest"></i></a>
            </div>';
            if($post['image'] != ''){
                $output .='<textarea style="min-height:130px" required name="content[]" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message...">'.$post['content'].'</textarea><span style="display:none;" class="county">0</span>';
            }else{
                $output .='<textarea style="min-height:auto" required name="content[]" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message...">'.$post['content'].'</textarea><span style="display:none;" class="county">0</span>';
            }

            if($post['image'] != ''){

                $output .= '<div class="image_uploader" style="display:none;">
                    <i class="ion-image"></i>
                </div>
                <div class="image_uploader2">
                    <label for="" class="custom-file-upload" style="display:none;">
                        <i class="ion-upload"></i>
                    </label>
                    <input class="social_image input_social_image" name="social_image[]" type="file"/>
                </div>
                <div class="image_uploader3" style="display:block;">
                    <i class="ion-close-circled"></i>
                    <img class="social_preview_image" src="'.$post['image'].'">
                </div>
                <div class="popup-content-image">
                    <img class="social_preview_image" src="'.$post['image'].'">
                </div>';
            }else{
                $output .= '<div class="image_uploader">
                    <i class="ion-image"></i>
                </div>
                <div class="image_uploader2">
                    <label for="" class="custom-file-upload">
                        <i class="ion-upload"></i>
                    </label>
                    <input class="social_image input_social_image" name="social_image[]" type="file"/>
                </div>
                <div class="image_uploader3">
                    <i class="ion-close-circled"></i>
                    <img class="social_preview_image" src="">
                </div>
                <div class="popup-content-image">
                    <img class="social_preview_image" src="">
                </div>';
            }

            $output .='<div class="counters flex-item">
                    <ul class="counters">
                        <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
                        <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
                        <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
                        <li class="score"><span>Score</span><span class="scores item_circle">
            ';
            if($score < 60){
                $output .='
                    <i class="ion-record grey_icon"></i>
                    <i class="ion-record grey_icon"></i>
                    <i class="ion-record grey_icon"></i></span></li>';
            }elseif($score >= 60 && $score <= 79){
                $output .='
                    <i class="ion-record firestorm"></i>
                    <i class="ion-record grey_icon"></i>
                    <i class="ion-record grey_icon"></i></span></li>';
            }elseif($score >= 80 && $score <= 89){
                $output .='
                    <i class="ion-record taser"></i>
                    <i class="ion-record taser"></i>
                    <i class="ion-record grey_icon"></i></span></li>';
            }elseif($score >= 90){
                $output .='
                    <i class="ion-record hulksmash"></i>
                    <i class="ion-record hulksmash"></i>
                    <i class="ion-record hulksmash"></i></span></li>';
            }


            if($i == 0){
                $output .= '<li class="delete_field"><i class="ion ion-minus-circled i_remove_filed" aria-hidden="true" title="Remove this field"></i> </li>';
            }else{
                $output .= '<li class="delete_field" style="display:block;"><i class="ion ion-minus-circled i_remove_filed" aria-hidden="true" title="Remove this field"></i> </li>';
            }


            $output .='</ul>
                </div>
        </div>';
        }
    }
    echo $output;
    wp_die();
}
add_action( 'wp_ajax_soopz_edit_post', 'soopz_edit_post' );


// Update Post
function soopz_update_post( ){
    if ( ! ( isset( $_REQUEST['action'] ) && 'soopz_update_post' == $_POST['action'] )  )
        return;

    $return = array(
        'error' => '1',
        'html' => 'Bad value'
    );

    if ( !is_user_logged_in() )
        exit( json_encode( $return ) );
    $post_id = $_POST['post_id'];

    // delete_field('field_599256824ec27',$post_id);

    $post_images = $_FILES['post_images'];
    $verify_images = $_POST['verify_images'];
    $user_id = get_current_user_id();
    $post_contents = $_POST['post_contents'];
    $social_medias = $_POST['social_medias'];
    // var_dump($verify_images);
    // exit;
    $names = $post_images['name'];
    $tmp_name = $post_images['tmp_name'];
    $type = $post_images['type'];
    $new_post = array(
        'ID' =>$post_id,
        'post_title'    => $post_contents[0],
    );

    // Insert the post into the database
    $post_id = wp_update_post( $new_post );
    $i=-1;
    $k=-1;
    foreach($post_contents as $content){ $i++;

        if($verify_images[$i] == 'null'){ 
            $attach_id = null;
        }elseif($verify_images[$i] == 'yes'){
            $attach_id = ' ';
        }else{ $k++;
            $filename = $names[$k];
            $wp_upload_dir = wp_upload_dir();
            move_uploaded_file( $tmp_name[$k], $wp_upload_dir['path']  . '/' . $filename );
            $attachment = array(
                'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ), 
                'post_mime_type' => $type[$k],
                'post_title' => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                'post_content' => '',
                'post_status' => 'inherit'
            );
            $filename = $wp_upload_dir['path']  . '/' . $filename;
            $attach_id = wp_insert_attachment( $attachment, $filename, $post_id );
            require_once( ABSPATH . 'wp-admin/includes/image.php' );
            $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
            wp_update_attachment_metadata( $attach_id, $attach_data );              
        }

        $final[] = array(
            'type'=>$social_medias[$i],
            'image'=>$attach_id,
            'content'=>$content,
        );
        if($attach_id == null){
            unset($final[$i]['image']);
        }
    }

    update_field('field_599256824ec27',$final,$post_id);
 
    
    if( $post_id ){
        $return = array(
            'error' => '0',
            'post_title' => $post_contents[0],
            'post_id' => $post_id,
            'html' => 'Your new post has been updated saved!',
        );
    } else {
        $return = array(
            'error' => '1',
            'post_title' => $post_contents[0],
            'html' => 'Cant Update Post!',
        );
    }

    echo json_encode( $return );
    exit;

}
add_action( 'wp_ajax_soopz_update_post', 'soopz_update_post' );

// Remove Post
function soopz_remove_post( ){
    if ( ! ( isset( $_REQUEST['action'] ) && 'soopz_remove_post' == $_POST['action'] )  )
        return;

    $return = array(
        'error' => '1',
        'html' => 'Bad value'
    );

    if ( !is_user_logged_in() )
        exit( json_encode( $return ) );

    $post_id = $_POST['post_id'];
    $user_id = get_current_user_id();
    $post_author_id = get_post_field( 'post_author', $post_id );

    if( get_post_type( $post_id ) == 'post' ){
        if( $user_id == $post_author_id ){
            if( current_user_can('delete_posts') || $user_id == $post_author_id ){
                wp_delete_post( $post_id, false );
                $return = array(
                    'error' => '0',
                    'html' => '<p> Post removed. </p>'
                );
            } else {
                $return = array(
                    'error' => '1',
                    'html' => '<p> You have not permission for delete posts!!! </p>'
                );
            }
        } else {
            $return = array(
                'error' => '1',
                'html' => 'You are not author of this post!!!'
            );
        }
    }
    echo json_encode( $return );
    exit;
}
add_action( 'wp_ajax_soopz_remove_post', 'soopz_remove_post' );


// update user data
function soopz_update_user_data( ){
    if ( ! ( isset( $_REQUEST['action'] ) && 'soopz_update_user_data' == $_POST['action'] )  )
        return;

    $return = array(
        'error' => '1',
        'html' => 'Bad value'
    );

    if ( !is_user_logged_in() )
        exit( json_encode( $return ) );

    $user_id = get_current_user_id();

    $new_user_data = array(
        'ID' => $user_id
    );

    if ( !empty( $_POST['user_pass'] ) ){
        if( strlen( $_POST['user_pass'] ) < 6 ){
            $return = array(
                'error' => '1',
                'html' => 'Password must be at least 6 characters!'
            );
            echo json_encode( $return ); exit;
        }
        $new_user_data['user_pass'] = $_POST['user_pass'];
    }

    if ( !empty( $_POST['user_email'] ) ){
        if( is_email( $_POST['user_email'] ) ){
            $new_user_data['user_email'] = $_POST['user_email'];
        } else {
            $return = array(
                'error' => '1',
                'html' => 'Invalid Email Address!'
            );
            echo json_encode( $return ); exit;
        }
    }
    if ( !empty( $_POST['first_name'] ) )
        $new_user_data['first_name'] = $_POST['first_name'];
    if ( !empty( $_POST['last_name'] ) )
        $new_user_data['last_name'] = $_POST['last_name'];

    //update user data
    $user_id = wp_update_user( $new_user_data );

    if ( is_wp_error( $user_id ) ) {
        $return = array(
            'error' => '1',
            'html' => 'There was an error!'
        );
    } else {
        $return = array(
            'error' => '0',
            'html' => 'Your data successfully updated!'
        );
    }
    echo json_encode( $return );
    exit;
}
add_action( 'wp_ajax_soopz_update_user_data', 'soopz_update_user_data' );




/*
 * The print_r function with <pre> tags for clear view
 */
if( !function_exists(i_print) ){
    function i_print($array){
        echo '<pre>'; print_r($array); echo '</pre>';
    }
}



// Get page link by template name
function wiser_page_link($page_name){
    $args = [
        'post_type' => 'page',
        'nopaging' => true,
        'meta_key' => '_wp_page_template',
        'meta_value' => $page_name.".php",
    ];
    $pages = get_posts( $args );
    foreach ( $pages as $page ){
        $page_id = $page->ID;
    }
    return get_page_link($page_id);
}

// Filter by status function
function soopz_filter_status_function(){
    $status_filter = $_POST['status_filter'];
    $date_filter = $_POST['date_filter'];
    $term_filter = $_POST['term_filter'];

    $user_id = get_current_user_id();
    $output = '';
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'author'=>$user_id,
        'order'=>$date_filter,
        's'=>$term_filter,
        'post_status'=>$status_filter,
    );
    $query = new WP_Query ( $args );
    while ($query -> have_posts()): $query -> the_post();
        $output .= soopz_one_post(get_the_ID(),get_the_title(),get_the_content());
    endwhile; wp_reset_query();
    echo $output;
    wp_die();
}
add_action( 'wp_ajax_soopz_filter_status', 'soopz_filter_status_function' );
add_action( 'wp_ajax_nopriv_soopz_filter_status', 'soopz_filter_status_function' );

// Filter by search function
function soopz_filter_search_function(){
    $status_filter = $_POST['status_filter'];
    $date_filter = $_POST['date_filter'];
    $term_filter = $_POST['term_filter'];

    $user_id = get_current_user_id();
    $output = '';
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'author'=>$user_id,
        's'=>$term_filter,
        'order'=>$date_filter,
        'post_status'=>$status_filter,
    );
    $query = new WP_Query ( $args );
    while ($query -> have_posts()): $query -> the_post();
        $output .= soopz_one_post(get_the_ID(),get_the_title(),get_the_content());
    endwhile; wp_reset_query();
    echo $output;
    wp_die();
}
add_action( 'wp_ajax_soopz_filter_search', 'soopz_filter_search_function' );
add_action( 'wp_ajax_nopriv_soopz_filter_search', 'soopz_filter_search_function' );

// Filter by date function
function soopz_filter_date_function(){
    $status_filter = $_POST['status_filter'];
    $date_filter = $_POST['date_filter'];
    $term_filter = $_POST['term_filter'];

    $user_id = get_current_user_id();
    $output = '';
    $args = array(
        'post_type' => 'post',
        'posts_per_page' => -1,
        'author'=>$user_id,
        'order'=>$date_filter,
        's'=>$term_filter,
        'post_status'=>$status_filter,
    );
    $query = new WP_Query ( $args );
    while ($query -> have_posts()): $query -> the_post();
        $output .= soopz_one_post(get_the_ID(),get_the_title(),get_the_content());
    endwhile; wp_reset_query();
    echo $output;
    wp_die();
}
add_action( 'wp_ajax_soopz_filter_date', 'soopz_filter_date_function' );
add_action( 'wp_ajax_nopriv_soopz_filter_date', 'soopz_filter_date_function' );



// One post for soopz to use
function soopz_one_post($post_id,$post_title,$post_content){
    return '<div class="nine columns post_row" style="margin-left: 0;">
        
        <div id="i_post_item_div_'.$post_id.'" class="post_item_row">
            <a target="_blanck" data-post-id="<?php echo $post_id;?>" class="i_post_edit" href="'.get_permalink($post_id).'?post_id='.$post_id.'">
                <span id="post_title_'.$post_id.'" class="post_text">'.$post_title.'</span>
                <i class="ion ion-compose i_post_edit" aria-hidden="true" data-post-id="'.$post_id.'"></i>
            </a>

        </div>
    </div>

    <div class="three columns">
        <div class="post_score">
            <i class="ion-record green_icon"></i><i class="ion-record green_icon"></i><i class="ion-record green_icon"></i>
        </div>
        <div class="post_date">
            <span>'.get_the_time('m/d/y').'</span>
          <i class="ion ion-close-circled wi-remove post_remove" data-post-id="'.$post_id.'" data-post-title="'.$post_title.'" title="Remove this post" aria-hidden="true"></i>
        </div>

    </div>';
}

// Option page function
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
    'key' => 'group_598790b78f204',
    'title' => 'Thmee Options',
    'fields' => array (
        array (
            'key' => 'field_598790c5eb5d3',
            'label' => 'Social Media Helpers',
            'name' => 'social_media',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'row',
            'button_label' => '',
            'sub_fields' => array (
                array (
                    'key' => 'field_598790d8eb5d4',
                    'label' => 'Kind',
                    'name' => 'kind',
                    'type' => 'select',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        'facebook' => 'Facebook',
                        'linkedin' => 'Linked In',
                        'twitter' => 'Twitter',
                        'instagram' => 'Instagram',
                        'googleplus' => 'Google Plus',
                        'pinterest' => 'Pinterest',
                    ),
                    'default_value' => array (
                    ),
                    'allow_null' => 0,
                    'multiple' => 0,
                    'ui' => 0,
                    'ajax' => 0,
                    'return_format' => 'value',
                    'placeholder' => '',
                ),
                array (
                    'key' => 'field_5987b3ecd59b4',
                    'label' => 'Content List',
                    'name' => 'con1',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => '',
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'row',
                    'button_label' => '',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_5987b414d59b5',
                            'label' => 'Title',
                            'name' => 'con11',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                        ),
                    ),
                ),
                array (
                    'key' => 'field_5987b456d59b6',
                    'label' => 'Image Description',
                    'name' => 'image_desc',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => 'wpautop',
                ),
                array (
                    'key' => 'field_5987b4cbd59b7',
                    'label' => 'Characters Description',
                    'name' => 'characters_desc',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => 'wpautop',
                ),
                array (
                    'key' => 'field_5987b4d8d59b8',
                    'label' => 'Hashtags Description',
                    'name' => 'hashtags_desc',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => 'wpautop',
                ),
                array (
                    'key' => 'field_5987b4e9d59b9',
                    'label' => 'Emoji Description',
                    'name' => 'emoji_desc',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => 'wpautop',
                ),
                array (
                    'key' => 'field_5987b4fdd59ba',
                    'label' => 'Trending Topics',
                    'name' => 'trend',
                    'type' => 'repeater',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'collapsed' => '',
                    'min' => 0,
                    'max' => 0,
                    'layout' => 'row',
                    'button_label' => '',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_5987b516d59bb',
                            'label' => 'Button Title',
                            'name' => 'trend1',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array (
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                        ),
                    ),
                ),
            ),
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'options_page',
                'operator' => '==',
                'value' => 'soopz_options',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;


if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array (
    'key' => 'group_5992566549e54',
    'title' => 'Posts',
    'fields' => array (
        array (
            'key' => 'field_599256824ec27',
            'label' => 'Posts',
            'name' => 'posts',
            'type' => 'repeater',
            'instructions' => '',
            'required' => 0,
            'conditional_logic' => 0,
            'wrapper' => array (
                'width' => '',
                'class' => '',
                'id' => '',
            ),
            'collapsed' => '',
            'min' => 0,
            'max' => 0,
            'layout' => 'row',
            'button_label' => '',
            'sub_fields' => array (
                array (
                    'key' => 'field_599256b34ec28',
                    'label' => 'Type',
                    'name' => 'type',
                    'type' => 'select',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'choices' => array (
                        'facebook' => 'Facebook',
                        'linkedin' => 'Linkedin',
                        'twitter' => 'Twitter',
                        'instagram' => 'Instagram',
                        'google-plus' => 'Google plus',
                        'pinterest' => 'Pinterest',
                    ),
                    'default_value' => array (
                    ),
                    'allow_null' => 0,
                    'multiple' => 0,
                    'ui' => 0,
                    'ajax' => 0,
                    'return_format' => 'value',
                    'placeholder' => '',
                ),
                array (
                    'key' => 'field_599256ec4ec2a',
                    'label' => 'Image',
                    'name' => 'image',
                    'type' => 'image',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'return_format' => 'url',
                    'preview_size' => 'thumbnail',
                    'library' => 'all',
                    'min_width' => '',
                    'min_height' => '',
                    'min_size' => '',
                    'max_width' => '',
                    'max_height' => '',
                    'max_size' => '',
                    'mime_types' => '',
                ),
                array (
                    'key' => 'field_59a4b2d306eee',
                    'label' => 'Content',
                    'name' => 'content',
                    'type' => 'textarea',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => '',
                ),
                array (
                    'key' => 'field_59f3238a7dc95',
                    'label' => 'Score',
                    'name' => 'score',
                    'type' => 'number',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array (
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'default_value' => '',
                    'placeholder' => '',
                    'maxlength' => '',
                    'rows' => '',
                    'new_lines' => '',
                ),


            ),
        ),
    ),
    'location' => array (
        array (
            array (
                'param' => 'post_type',
                'operator' => '==',
                'value' => 'post',
            ),
        ),
    ),
    'menu_order' => 0,
    'position' => 'acf_after_title',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => 1,
    'description' => '',
));

endif;