<?php
/*
Template Name: Login
*/
?>
<?php
if ( is_user_logged_in() ) {
    wp_redirect(get_permalink(8));
}

get_header();
?>
<div id="wrapper">
    <div id="content_login" class="login_page">
        <div id="container_login">
            <div class='login_page_logo'>
                <?php
                $image_id = get_field("login_page_logo");
                $image_url = wp_get_attachment_url($image_id);
                ?>
                <img src='<?php echo $image_url ?>'>
            </div>

            <div class='login_page_headline'>
                <?php
                //                        $headline = get_field("login_page_headline");
                ?>
                <!--<h1><?php // echo $headline;  ?></h1>-->
            </div>
            <?php
            if (!is_user_logged_in()) {
                $login_redirect_url = ( $_GET['redirect_url'] ) ? $login_redirect_url = $_GET['redirect_url'] : get_permalink(8);
            ?>
            <div class="sign_in_form">
                <!--<form action="">
                    <input type="text" name="email" placeholder="Email">
                    <input type="password" name="password" placeholder="Password">
                </form>-->
                <form id="i_login_form" action='<?php echo esc_url( home_url( '', $scheme ) ); ?>/wp-login.php' method="post">
                    <p class="status"> &nbsp; </p>
                    <div class="i_input_helper_div clearfix">
                        <label>
                            <input type='text' name='username' id='username' value='<?php echo esc_attr($user_login); ?>' size='20' placeholder="Username / Email Address" class="form_control bord_left" />
                        </label>
                    </div>
                    <div class="i_input_helper_div clearfix">
                        <label>
                            <input type='password' name='password' id='password' size='28' placeholder="Password" class="form_control bord_left" />
                        </label>
                    </div>
                    <div class="login_submit_div clearfix">
                        <input class="submit_button etlogin-button btn btn-primary" type="submit" value="Login" name="submit">
                        <label class="remember_me_div">
                            <span><?php _e('Remember me'); ?></span>
                            <input name="rememberme" id="rememberme" value="forever" type="checkbox">
                        </label>
                    </div>
                    <div class="lost_password_div">
                        <a class="lost" href="<?php echo wp_lostpassword_url(); ?>"><?php _e('Lost your password ?') ?></a>
                    </div>
                    <input type="hidden" value="<?php echo $login_redirect_url; ?>" id="login_redirect_url">
                    <?php wp_nonce_field( 'ajax-login-nonce', 'security' ); ?>
                </form>
            </div>
            <div class='login_page_sign_buuttons'>
                <div class="connect_other_account">
                    <div class="conect_with_other_social">
                        <div class="all_social_buttons">
                            <div class='google_plus_button'>
                                <a href="http://soopz.com/wp-login.php?loginGoogle=1&redirect=http://soopz.com"
                                   class="button google-plus-button"
                                   onclick="window.location = 'http://soopz.com/wp-login.php?loginGoogle=1&redirect='+window.location.href; return false;">Google+</a>
                            </div>
                            <div class='facebook_button'>
                                <a href="http://soopz.com/wp-login.php?loginFacebook=1&redirect=http://soopz.com"
                                   class="button facebook-button"
                                   onclick="window.location = 'http://soopz.com/wp-login.php?loginFacebook=1&redirect='+window.location.href; return false;">Facebook</a>
                            </div>
                            <div class='twitter_button'>
                                <a href="http://soopz.com/wp-login.php?loginTwitter=1&redirect=http://soopz.com"
                                   class="button twitter-button"
                                   onclick="window.location = 'http://soopz.com/wp-login.php?loginTwitter=1&redirect='+window.location.href; return false;">Twitter</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="register_new_account">
                    <p>New here?<a href="#"> Register</a> an account.</p>
                </div>

                <?php
                } else {
                    echo 'You already logged in';

                    //$user_info = get_user_meta( get_current_user_id() );
                    //$user_profile_picture = get_user_meta( get_current_user_id(), 'fb_profile_picture', true );
//                            echo '<pre>';print_r( $user_info );
                }
                ?>
            </div>

        </div>
    </div>
</div>
<?php get_footer(); ?>
