<?php
/*
Template Name: Campaign
*/

if ( !is_user_logged_in() ){
    wp_redirect( home_url() );
}

$user_id = get_current_user_id();

get_header();
?>
	<div class="campaign_page">
	    <div class="word-count editor_page i_post_input_item_div">
	        <h4>New Campaign</h4>
	        <p>Use the outline below to create dozens of different message variations for your piece of promoted content.</p>
	    </div>
	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

	    <div class="flex-wrapper soopz-input campaign_input">
	        <span class="campaign_icon"><i class='ion-compose'></i></span>
	            <textarea  rows="1" id="i_post_content" name="content" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
	            <div class="counters flex-item">
	                <ul class="counters">
	                    <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
	                    <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
	                    <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
	                    <li class="score"><span>Score</span><span class="scores item_circle"><i class="ion-record firestorm"></i> <i class="ion-record taser"></i> <i class="ion-record hulksmash"></i></span></li>
	                </ul>
	            </div>
	    </div>

		<div class="campaign_images">
			<div class="block_image">
				<div class="inner_image">
		            <i class='ion-image'></i>
		            <h3><b>Image:</b> Landscape</h3>
				</div>
	            <p>Recommended: 1200x628</p>
			</div>
			<div class="block_image">
				<div class="inner_image">
		            <i class='ion-image'></i>
		            <h3><b>Image:</b> Square</h3>
				</div>
	            <p>Recommended: 1080x1080</p>
			</div>
			<div class="block_image">
				<div class="inner_image">
		            <i class='ion-image'></i>
		            <h3><b>Image:</b> Portrait</h3>
				</div>
	            <p>Recommended: 735x1102</p>
			</div>
		</div>

	    <div class="textare_buttons i_relative">
	    	<span id="btn-save" class="button" title="Save">Save</span>
	        <span id="btn-generate" class="button" title="Generate">Generate</span>
	    </div>
	</div>
<?php get_footer(); ?>