<!DOCTYPE html>

<html lang="en">

<head>

    <?php wp_head(); ?>

    <!-- Basic Page Needs

    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <meta charset="utf-8">

    <meta name="description" content="">

    <meta name="author" content="Warfare Plugins">

    <!-- Mobile Specific Metas

    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <meta name="viewport" content="width=device-width, initial-scale=1">



    <!-- FONT

    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <link href="//fonts.googleapis.com/css?family=Lato:300,400,700,300italic,700italic" rel="stylesheet"

          type="text/css">

    <link href="//code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css">




    <!-- Javascript

    ––––––––––––––––––––––––––––––––––––––––––––––––––  -->

    <script>
        
        
        
//        function textAreaAdjust(o) {
//       //     setTimeout(1000);
//            
//            o.style.height = "1px";
////        console.log(o.scrollHeight);
//           o.style.height = (0 + o.scrollHeight) + "px";
//
//
//
//
//        }
        
  


    </script>

    <script>

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){

        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),

        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)

        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');



        ga('create', 'UA-55847042-4', 'auto');

        ga('send', 'pageview');



    </script>



    <!-- Favicon

    –––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <link rel="icon" type="image/png" href="images/favicon.png">

    <!-- <link href="css/basicPopupDark.css" rel="stylesheet"> -->

    <!-- <link href="css/basicPopupClear.css" rel="stylesheet"> -->

    <!--<script src="<?php /*echo get_stylesheet_directory_uri(); */ ?>/script.js"></script>-->

</head>

<body <?php body_class(); ?>>



<!-- Primary Page Layout

–––––––––––––––––––––––––––––––––––––––––––––––––– -->

<?php

if ( is_user_logged_in() ) {

    global $i_user_info;

    ?>

    <div class="header twelve columns">

        <nav class="navbar">

            <div class="nav-container">

                <ul class="navbar-list logo">

                    <li class="navbar-item nine columns">

                        <a href="<?php echo wiser_page_link('page-editor_template') ?>" class="navbar-link"><img

                                    src="<?php bloginfo('template_directory'); ?>/images/logo.png"/></a>

                    </li>

                </ul>

                <div class="header-menu">
                    <?php wp_nav_menu(array(
                        'theme_location'    => 'header_menu',
                        'menu' => '',
                    )); ?>
                </div>

                <div class="user-menu two columns">

                    <div class="user_data">

                        <?php

                        $user_name = $i_user_info['user_name'];

                        $user_last_name = $i_user_info["last_name"];

                        $display_name = $i_user_info['display_name'];

                        $user_id = $i_user_info['user_id'];

                        ?>

                        <div>

                            <span class="profile-username"><?php echo $display_name; ?></span>

                        </div>

                        <div>

                            <img src="<?php echo $i_user_info['profile_picture']; ?>">

                        </div>

                    </div>

                    <?php wp_nav_menu(array(

                        'theme_location'    => 'main_menu',

                        'menu' => 'Header account menu'

                    )); ?>

                </div>

            </div>

        </nav>

    </div>

    <?php

}

?>


<?php if( have_rows('social_media','option') ): while ( have_rows('social_media','option') ) : the_row();
$kind = get_sub_field('kind');
 ?>

<div class="social_active_block social_active_block_<?php echo $kind; ?>">
    <div class="social_<?php echo $kind; ?> social_top_block">
        <i class='ion-social-<?php echo $kind; ?>'></i>
    </div>
    <div class="social_block social_content">
        <i class='ion-ios-compose main_icon_content'></i>
        <h3>Content</h3>
        <div class="content_icons">
            <i class='ion-record grey_icon'></i>
            <i class='ion-record grey_icon'></i>
            <i class='ion-record grey_icon'></i>
        </div>
        <ul>
            <?php if( have_rows('con1') ): while ( have_rows('con1') ) : the_row(); ?>
                        <li><i class='ion-checkmark-circled grey_icon_borders'></i><?php the_sub_field('con11') ?></li>
            <?php endwhile; else : endif; ?>
        </ul>
    </div>


    <div class="social_block social_image">
        <i class='ion-image main_icon_content'></i>
        <h3>Image</h3>
        <div class="content_icons">
            <i class='ion-record red_icon'></i>
            <i class='ion-record grey_icon'></i>
            <i class='ion-record grey_icon'></i>
        </div>
        <?php the_sub_field('image_desc') ?>
    </div>

    <div class="social_block social_characters">
        <i class='ion-android-locate main_icon_content'></i>
        <h3>Characters</h3>
        <div class="content_icons">
            <i class='ion-record grey_icon'></i>
            <i class='ion-record grey_icon'></i>
            <i class='ion-record grey_icon'></i>
        </div>
        <?php the_sub_field('characters_desc') ?>
    </div>

    <div class="social_block social_hashtags">
        <i class='ion-pound main_icon_content'></i>
        <h3>Hashtags</h3>
        <div class="content_icons">
            <i class='ion-record green_icon'></i>
            <i class='ion-record green_icon'></i>
            <i class='ion-record green_icon'></i>
        </div>
        <?php the_sub_field('hashtags_desc') ?>
    </div>


    <div class="social_block social_emoji">
        <i class='ion-android-happy main_icon_content'></i>
        <h3>Emoji</h3>
        <div class="content_icons">
            <i class='ion-record grey_icon'></i>
            <i class='ion-record grey_icon'></i>
            <i class='ion-record grey_icon'></i>
        </div>
        <?php the_sub_field('emoji_desc') ?>
    </div>

    <div class="social_block social_trending">
        <i class='ion-arrow-graph-up-right main_icon_content'></i>
        <h3>Trending Topics</h3>
        <div class="social_buttons">
            <?php if( have_rows('trend') ): while ( have_rows('trend') ) : the_row(); ?>
                        <button class='social_<?php echo $kind; ?>'><?php the_sub_field('trend1') ?></button>
            <?php endwhile; else : endif; ?>
        </div>
    </div>

    <div class="social_block ask_questions">
        <i class='ion-help-circled main_icon_content'></i>
        <h3>Ask a question</h3>
        <div class="social_buttons">
<?php
$args = array(
    'post_type' => 'question',
    'posts_per_page' => 3,
    'cache_results'  => false,
    'orderby' => 'random',
    'no_found_rows'=>true,
    'tax_query' => array(
    array(
    'taxonomy' => 'question-category',
    'field'    => 'name',
    'terms'    => ucfirst($kind),
    ),
    ),
                    );
$query = new WP_Query ( $args );
while ($query -> have_posts()): $query -> the_post();
?>

            <button class='social_<?php echo $kind; ?>'><?php the_title(); ?></button>
<?php endwhile; wp_reset_query(); ?>
        </div>
    </div>


    <div class="social_block opt_tips">
        <h3>Optimization Tips</h3>

<?php
$args = array(
    'post_type' => 'tip',
    'posts_per_page' => 3,
    'cache_results'  => false,
    'orderby' => 'random',
    'no_found_rows'=>true,
    'tax_query' => array(
    array(
    'taxonomy' => 'tip-category',
    'field'    => 'name',
    'terms'    => ucfirst($kind),
    ),
    ),
                    );
$query = new WP_Query ( $args );
while ($query -> have_posts()): $query -> the_post();
?>

            <p><?php the_title(); ?></p>
<?php endwhile; wp_reset_query(); ?>

    </div>

</div>
        
<?php endwhile; else : endif; ?>
<?php

if( !is_home() && !is_front_page() ){

?>
<div class="container">

    <div class="row">

    <?php

}

?>
