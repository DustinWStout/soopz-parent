<?php
if( !is_home() && !is_front_page() ){
?>
</div>
</div>
    <?php
}
?>
<footer>
    <div class="container">
        <div class="row">
            <div class="credits">&copy; Copyright <?php echo date("Y") ?> &middot; a <a href="https://warfareplugins.com" class="battleship" target="_blank">Warfare Plugins</a> (Beta) Project.</div>
        </div>
    </div>

</footer>



<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
<?php wp_footer(); ?>
</html>
