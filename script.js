jQuery( document ).ready( function($) {
    // misc functions
    $(".user_data.user_name").focus(function(){
        console.log(1111);
        $(".user_data.form_submit").css("display","block");
    });
    $(".user_data.user_last_name").focus(function(){
        $(".user_data.form_submit").css("display","block");
    });
    $(".user_data.user_email").focus(function(){
        $(".user_data.form_submit").css("display","block");
    });
    $(".user_data.user_pass").focus(function(){
        $(".user_data.form_submit").css("display","block");
    });

    $('.header-menu ul li.current_page_item:first-child a').addClass('first_item_box_shadow');
    if($('.header-menu ul li:last-child').hasClass('current_page_item')){
        $('.header-menu ul li:first-child a').addClass('first_item_box_shadow');
    }else{
        $('.header-menu ul li:nth-child(2) a').addClass('last_item_box_shadow');
    }

    



    var info = {}; var t_el; var post_id;
    var can_submitted = true;
    var i_loading = $('#i_loading');

    //  Login function
    var i_login_form = 'form#i_login_form';
    $( i_login_form ).on('submit', function(e){
        $('form#i_login_form p.status').show().text( i_infos.loadingmessage );
        var rememberme = 0; if( $( i_login_form+' #rememberme' ).is(':checked') ) rememberme = 1;
        $.ajax({
            type: 'POST',
            dataType: 'json',
            url: i_infos.ajax_url,
            data: {
                'action': 'i_ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
                'username': $( i_login_form+' #username' ).val(),
                'password': $( i_login_form+' #password' ).val(),
                'security': $( i_login_form+' #security' ).val(),
                'rememberme': rememberme,
                //'redirect_url' : $( '#login_redirect_url' ).val()
            },
            success: function(data){
                $( i_login_form+' p.status' ).text(data.message);
                if (data.loggedin == true){
                    if( $( '#login_redirect_url' ).val() != '' )i_infos.redirecturl = $( '#login_redirect_url' ).val();
                    document.location.href = decodeURI( i_infos.redirecturl );
                }
            }
        });
        e.preventDefault(); return false;
    });
    $('#i_loading').hide();


// ==================================================================
    //CRUD functions for post


    $('body').on('keydown', '.i_post_editor_input', i_remove_required );
    function i_remove_required(){
        $(this).removeClass('required_field');
    }

    // create post
    $('body').on('click', '#btn-save', soopz_save_post);
    function soopz_save_post() {
        var empty = false;
        $('.i_post_editor_input').each(function() {
            if($(this).val() == ''){
                $(this).addClass('required_field');
                empty = true;
            }
        });
        if(empty == true){
            return false;
        }

        var info = new FormData();

        $('.i_post_editor_input').each(function() {
            var value = $(this).val();
            info.append('post_contents[]',value);
        });

        $('.social_active_type').each(function() {
            var social = $(this).attr('social_media');
            info.append('social_medias[]',social);
        });

        $('.county').each(function() {
            var score = $(this).text();
            info.append('scores[]',score);
        });

        
        info.append( 'action', 'soopz_save_post');

        $('.input_social_image').each(function() {
            if($(this).prop("files")[0] !== undefined){
                info.append('verify_images[]','image');
            }else{
                info.append('verify_images[]','no');
            }
            info.append( 'post_images[]', $(this).prop("files")[0]);
        });
        i_loading.show();
        jQuery.ajax({
            type: 'POST',
            url: i_infos.ajax_url,
            data: info,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                var data = jQuery.parseJSON(data);
                var new_post_html = '<div id="i_post_item_div_'+data.post_id+'" class="post_item_row new_post_item_row">';
                new_post_html+= '<a target="_blanck" data-post-id="'+data.post_id+'" class="i_post_edit" href="'+i_infos.post_edit_permalink+'?post_id='+data.post_id+'">';
                new_post_html+= '<span id="post_title_'+data.post_id+'" class="post_text">'+data.post_title+'</span>';
                new_post_html+= '<i class="ion ion-compose i_post_edit" aria-hidden="true" data-post-id="'+data.post_id+'"></i>';
                new_post_html+= '</a> <i class="ion ion-close-circled wi-remove i_post_remove" data-post-id="'+data.post_id+'" data-post-title="'+data.post_title+'" title="Remove this post" aria-hidden="true"></i>';
                new_post_html+= '</div>';
                $('#i_posts_list').prepend( new_post_html );

                $(".i_post_actions_msg").text( data.html ).show().delay(1500).fadeOut( 1000 );

                setTimeout(function () {
                    var new_txt_html = $('.all_soopz_inputs').find('.soopz-input:last-child').clone(true).find('textarea').val('').parent().find('.select_social_type i').removeClass().addClass('ion-android-person social_active_type social_profile').attr('social_media','').closest('.soopz-input');
                    $('.item_circle:not(:eq(3))',new_txt_html).html('0');
                    $('#social_image',new_txt_html).val('');
                    $('.image_uploader3,.image_uploader2',new_txt_html).hide();
                    $('.image_uploader',new_txt_html).show();
                    $('.delete_field',new_txt_html).hide();
                    $('.all_soopz_inputs .soopz-input').remove();
                    $(new_txt_html).appendTo( ".all_soopz_inputs" );
                    autosize($('.autoExpand2'));
                }, 1500);
                can_submitted = true;
                i_loading.hide();
            },

            error: function(MLHttpRequest, textStatus, errorThrown) {
                // alert(errorThrown);
            }

        });

    };

    // read post
    $('body').on('click', '.i_post_edit', i_post_edit );
    function i_post_edit(e){
        e.preventDefault();
        var post_id = $(this).data('post-id');

        var info = new FormData();
        info.append( 'action', 'soopz_edit_post');
        info.append( 'post_id', post_id);
        
        i_loading.show();
        jQuery.ajax({
            type: 'POST',
            url: i_infos.ajax_url,
            data: info,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                i_loading.hide();
                $("#btn-save").hide();
                $("#btn_update_post,#btn_cancel_action").show();
                $(".all_soopz_inputs .soopz-input").remove();
                $('.all_soopz_inputs').html(data);
                $("html, body").animate({ scrollTop: 0 }, "slow");
                wordCounter.start();
                wordCounter.initialize('.word-count');
                autosize($('.autoExpand2'));
                $("#i_post_id").val(post_id);

            },

            error: function(MLHttpRequest, textStatus, errorThrown) {
                // alert(errorThrown);
            }

        });
    }


    //update post
    $('body').on('click', '#btn_update_post', soopz_update_post );
    function soopz_update_post(){
        var info = new FormData();


        var post_id = $('#i_post_id').val();
        $('.i_post_editor_input').each(function() {
            var value = $(this).val();
            info.append('post_contents[]',value);
        });

        $('.social_active_type').each(function() {
            var social = $(this).attr('social_media');
            info.append('social_medias[]',social);
        });
        
        info.append( 'action', 'soopz_update_post');
        info.append( 'post_id', post_id);


        $('.input_social_image').each(function() {
            if($(this).attr('no-img') == 'yes' && $(this).prop("files")[0] == undefined){
                info.append('verify_images[]','yes');
            }else if($(this).prop("files")[0] !== undefined){
                info.append('verify_images[]','no');
            }else{
                info.append('verify_images[]','null');
            }
            info.append( 'post_images[]', $(this).prop("files")[0]);
        });


        // info.append( 'verify_images', JSON.stringify(verify_images));
        i_loading.show();
        jQuery.ajax({
            type: 'POST',
            url: i_infos.ajax_url,
            data: info,
            processData: false,
            contentType: false,
            cache: false,
            success: function(data) {
                var data = jQuery.parseJSON(data);
                var new_post_html = '<div id="i_post_item_div_'+data.post_id+'" class="post_item_row new_post_item_row">';
                new_post_html+= '<a target="_blanck" data-post-id="'+data.post_id+'" class="i_post_edit" href="'+i_infos.post_edit_permalink+'?post_id='+data.post_id+'">';
                new_post_html+= '<span id="post_title_'+data.post_id+'" class="post_text">'+data.post_title+'</span>';
                new_post_html+= '<i class="ion ion-compose i_post_edit" aria-hidden="true" data-post-id="'+data.post_id+'"></i>';
                new_post_html+= '</a> <i class="ion ion-close-circled wi-remove i_post_remove" data-post-id="'+data.post_id+'" data-post-title="'+data.post_title+'" title="Remove this post" aria-hidden="true"></i>';
                new_post_html+= '</div>';
                $("#i_posts_list").find('[data-post-id='+post_id+']').parent().remove();
                $('#i_posts_list').prepend( new_post_html );

                $(".i_post_actions_msg").text( data.html ).show().fadeOut( 1500 );
                setTimeout(function () {
                    var new_txt_html = $('.all_soopz_inputs').find('.soopz-input:last-child').clone(true).find('textarea').val('').parent().find('.select_social_type i').removeClass().addClass('ion-android-person social_active_type social_profile').attr('social_media','').closest('.soopz-input');
                    $('.item_circle:not(:eq(3))',new_txt_html).html('0');
                    $('#social_image',new_txt_html).val('');
                    $('.image_uploader3,.image_uploader2',new_txt_html).hide();
                    $('.image_uploader',new_txt_html).show();
                    $('.delete_field',new_txt_html).hide();
                    $('#btn_update_post,#btn_cancel_action').hide();
                    $('#btn-save').show();
                    $('.all_soopz_inputs .soopz-input').remove();
                    $(new_txt_html).appendTo( ".all_soopz_inputs" );
                    autosize($('.autoExpand2'));
                }, 1500);
                can_submitted = true;
                i_loading.hide();
            },

            error: function(MLHttpRequest, textStatus, errorThrown) {
                // alert(errorThrown);
            }

        });
    }

    //remove post
    $('body').on('click', '.i_post_remove', i_post_remove );
    function i_post_remove(){
        var post_id = $(this).data('post-id');
        var post_title = $(this).data('post-title');
        t_el = $(this);

        var delete_message = "Are you sure, you want remove the post <b> \n'"+post_title+"'</b> \n?<br> This cannot be undone.";

        $.confirm({
            title: 'Delete forever ?',
            content: delete_message,
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            buttons: {
                confirm:{ 
                    btnClass: 'btn-blue',
                    text: 'YES, DELETE',
                    action:function () {
                    i_loading.show();
                    //Send ajax
                    info = {};
                    info['action'] = 'soopz_remove_post';
                    info['post_id'] = post_id;

                    //i_loading.show();
                    $.post( i_infos.ajax_url, info ).done(function(data) {
                        data = jQuery.parseJSON( data );
                        if( data.error == 0 ){
                            $(t_el).parents('.post_item_row').fadeOut( 400, function(){ $(this).remove(); });
                        } else {
                            alert( data.html );
                        }
                        can_submitted = true;
                        i_loading.hide(); //$.fancybox.close();
                    });
                }
                },
                cancel:{
                    btnClass: 'btn-red',
                    text: 'NO, CANCEL',
                }
            }
        });

    }


    //Cancel updating post
    
    $('body').on('click', '#btn_cancel_action', btn_cancel_action );
    function btn_cancel_action(){
        $("#btn-save").show();
        $("#btn_update_post,#btn_cancel_action").hide();
        var new_txt_html = $('.all_soopz_inputs').find('.soopz-input:last-child').clone(true).find('textarea').val('').parent().find('.select_social_type i').removeClass().addClass('ion-android-person social_active_type social_profile').attr('social_media','').closest('.soopz-input');
        $('.item_circle:not(:eq(3))',new_txt_html).html('0');
        $('#social_image',new_txt_html).val('');
        $('.image_uploader3,.image_uploader2',new_txt_html).hide();
        $('.image_uploader',new_txt_html).show();
        $('.delete_field',new_txt_html).hide();
        $('.all_soopz_inputs .soopz-input').remove();
        $(new_txt_html).appendTo( ".all_soopz_inputs" );
        autosize($('.autoExpand2'));
    }
// ==================================================================
    // Delete user account
    $('body').on('click', '.delete_acc', delete_acc );
    function delete_acc(e){
        var delete_message = "Are you sure, you want remove your account \n?<br> This cannot be undone.";
        e.preventDefault();
        $.confirm({
            title: 'Delete forever ?',
            content: delete_message,
            type: 'red',
            typeAnimated: true,
            boxWidth: '30%',
            useBootstrap: false,
            buttons: {
                confirm:{ 
                    btnClass: 'btn-blue',
                    text: 'YES, DELETE',
                    action:function () {
                        var url = document.location.href+"?delete_acc=yes";
                        document.location = url;
                }
                },
                cancel:{
                    btnClass: 'btn-red',
                    text: 'NO, CANCEL',
                }
            }
        });
    }


// ==================================================================
    //Tab KeyDown
    $('body').keydown(function(e) {
        var code = e.keyCode || e.which;
        if ( code == '9' ) {
            //console.log( document.activeElement );
            if( $('.i_post_editor_input').is(':focus') ){
                var focusedIndex; var last_index = $('.i_post_editor_input').length-1;
                var focusedItem = $('.i_post_editor_input:focus')[0];
                $('.i_post_editor_input').each( function(index, el){
                    if( el == focusedItem )
                        focusedIndex = index;
                });
                if( focusedIndex == last_index ){ //I am last input
                    $( ".new_textarea" ).click();
                }
            }
        }
    });


// ==================================================================
    //Remove input
    $('body').on('click', '.i_remove_filed', i_remove_filed );
    function i_remove_filed(){
        $(this).parents('.soopz-input').fadeOut( 100, function(){ $(this).remove(); });
    }

// ==================================================================
    //update user data
    $('#update_user_data').submit( soopz_update_user_data );
    function soopz_update_user_data(){

        info = {};
        info['action'] = 'soopz_update_user_data';
        info['first_name'] = $('#user_firstname').val();
        info['last_name'] = $('#user_lastname').val();
        info['user_email'] = $('#user_email').val();
        info['user_pass'] = $('#user_pass').val();
        //info['post_content'] = post_content;

        i_loading.show();
        $.post( i_infos.ajax_url, info ).done(function(data) {
            data = jQuery.parseJSON( data );
            if( data.error == 0 ){
                $("#i_update_user_data_msg").text( data.html ).show().fadeOut( 2000 );
            } else {
                alert( data.html );
            }
            can_submitted = true;
            i_loading.hide(); //$.fancybox.close();
        });

        event.preventDefault();
        return false;
    }

// ==================================================================
    // copy to clipboard
    $('#copy_to_clipboard').click( copy_to_clipboard );
    function copy_to_clipboard(){
        var copy_html = $('#i_post_content').val();
        $('.i_post_content_item').each( function ( index, el){
            if( $(el).val() ){
                copy_html+= '\n\n'+$(el).val();
            }
        });
        if( copy_html ){
            $('#copy_to_clipboard_area').val( copy_html ).click();
            $(this).focus();
        } else {
            alert( 'Post content is empty!');
        }
    }

    $('#copy_to_clipboard_area').click(function() {
        $(this).focus();
        $(this).select();

        try {
            document.execCommand('copy');// copy text
        }
        catch (err) {
            alert('Please press Ctrl/Cmd+C to copy!');
        }
        $(".copied").text("Copied to clipboard").show().fadeOut(1200);
    });
    

// ==================================================================
    // Textarea functions
    $('body').on('click', '.select_social_type',  function(e) {
        $(this).css('visibility', 'hidden');
        $(this).next().show();
    });

    $('body').on('click', '.social_types a',  function(e) {
        e.preventDefault();
        var social_media_class = $(this).find('i').attr('class');
        var social_media_name = $(this).attr('title');
        var social_media_name = social_media_name.toLowerCase();
        $(this).parent().prev().find('i').removeClass().addClass(social_media_class+' social_active_type white_icon').attr('social_media',social_media_name);
        $(this).parent().toggle();
        $('.select_social_type').css('visibility', 'visible');
    });

    $('body').on('focus', '.i_post_editor_input',  function(e) {
        var social_media_name = $(this).parent().find('.select_social_type i').attr('social_media');
        $('.social_active_block').not('.social_active_block_'+social_media_name).hide();
        $('.social_active_block').not('.social_active_block_'+social_media_name).css('right','-250px');
        $('.social_active_block_'+social_media_name).show().animate({
            right:'0',
        },250);
        $('body').css('overflow-x','hidden');
        if($('.social_active_block_'+social_media_name).is(":visible")){
            $('.container').animate({
                'padding-right':'100px',
            },250);
        }else{
            $('.container').animate({
                'padding-right':'0px',
            },250);
        }


    });

    // $('body').on('blur', '.i_post_editor_input',  function(e) {
    //     $('.social_active_block').css('right','-250px').hide();
    //     $('.container').animate({
    //         'padding-right':'0px',
    //     },250);
    // });


    $('body').on('click', '.custom-file-upload',  function(e) {
        $(this).next().click();
    });

    $('body').on('change', '.social_image',  function(e) {
        get_image_on_click(this);
        $(this).parent().hide();
        $(this).parent().next().show();
        var our_textarea = $(this).parent().parent().find('.i_post_editor_input').css('min-height','130px');
        var fileName = this.files[0].name;

        // Image validation
        var county = $(this).parent().parent().find('.county');
        county.attr('has_image','true');
        var new_county = parseInt(county.text()) + 10;
        county.text(new_county);
        

        $('.social_content ul li:nth-child(2) i').removeClass().addClass('ion-checkmark-circled green_icon_borders');


    });


    $('body').on('click', '.image_uploader',  function(e) {
        $(this).hide();
        $(this).next().show();
        $(this).next().find('.custom-file-upload').show();
    });




    $('body').on('click', '.image_uploader3 img',  function(e) {
        $.basicpopup({
            content: $(this).parent().parent().find('.popup-content-image').html(),
            mainClass : 'popup-preview-image',
        });
    });

    $('body').on('click', '.image_uploader3 i',  function(e) {
        $(this).parent().parent().find($('.social_image')).val("");
        $(this).parent().parent().find($('.social_preview_image')).attr('src','');
        $(this).parent().parent().find($('.image_uploader3')).hide();
        $(this).parent().parent().find($('.image_uploader2,.image_uploader2 label')).show();
        $(this).parent().parent().find($('.i_post_editor_input')).css('min-height','auto');
        $(this).parent().parent().find($('.input_social_image')).attr('no-img','yes');

        // Image validation
        var county = $(this).parent().parent().find('.county');
        county.attr('has_image','false')
        var new_county = parseInt(county.text()) - 10;
        county.text(new_county);

        $('.social_content ul li:nth-child(2) i').removeClass().addClass('ion-checkmark-circled grey_icon_borders');

    });

    autosize($('.autoExpand2'));

    $('.new_textarea').click(function(){
        var new_txt_html = $(this).prev().find('.soopz-input:last-child').clone(true).find('textarea').val('').parent().find('.select_social_type i').removeClass().addClass('ion-android-person social_active_type social_profile').attr('social_media','').closest('.soopz-input');
        $('.item_circle:not(:last-child)',new_txt_html).html('0');
        $('.input_social_image',new_txt_html).val('').attr('no-img','');
        $('.image_uploader3,.image_uploader2',new_txt_html).hide();
        $('.image_uploader',new_txt_html).show();
        $('.delete_field',new_txt_html).show();
        $(new_txt_html).appendTo( ".all_soopz_inputs" );
        autosize($('.autoExpand2'));
        $('.county',new_txt_html).text(0).removeAttr('has_text has_url char_count has_image no_hashtag has_emoji');
        $('.scores i:nth-child(1),.scores i:nth-child(2),.scores i:nth-child(3)',new_txt_html).removeClass().addClass('ion-record grey_icon');


    });

// ==================================================================
    // Filter posts functions
    $('.select_filter').niceSelect();


    $('.scores_filter ul li:nth-child(2)').html('<span class="ion-record green_icon"></span><span class="ion-record green_icon"></span><span class="ion-record green_icon"></span>');

    $('.scores_filter ul li:nth-child(3)').html('<span class="ion-record blue_icon"></span><span class="ion-record blue_icon"></span><span class="ion-record grey_icon"></span>');

    $('.scores_filter ul li:nth-child(4)').html('<span class="ion-record red_icon"></span><span class="ion-record grey_icon"></span><span class="ion-record grey_icon"></span>');

    $('.scores_filter ul li:nth-child(5)').html('<span class="ion-record grey_icon"></span><span class="ion-record grey_icon"></span><span class="ion-record grey_icon"></span>');
    $('.scores_filter ul li').click(function(){
        var selected = $(this).html();
        setTimeout(function () {
                $('.scores_filter .select_filter span.current').html(selected);
            }, i * 200);
    });


    $('.types_filter select').change(function(e){
        var types_filter = $('.types_filter select').val();
        e.preventDefault();
        if(types_filter == ''){
            $('.types_filter .list li:first-child').hide();
        }else{
            $('.types_filter .list li:first-child').show();
        }
    });




    $('.status_filter select').change(function(e){
        var status_filter = $('.status_filter select').val();
        var term_filter = $('.search').val();
        var date_filter = $('.date_filter').attr('date-filter');

        e.preventDefault();
        if(status_filter == ''){
            var status_filter = 'any';
            $('.status_filter .list li:first-child').hide();
        }else{
            $('.status_filter .list li:first-child').show();
        }
        i_loading.show();
        $.ajax({
        type: "POST",
        url: i_infos.ajax_url,
        data: { action: 'soopz_filter_status', status_filter: status_filter,term_filter:term_filter,date_filter:date_filter},
        success: function(response){
        i_loading.hide();
        $("#all_filtered_posts").html(response);
        },
        error: function(error){
        console.log("bad");
        }
        });
        
    });


    $('.search_filter').click(function(e){
        var status_filter = $('.status_filter select').val();
        var term_filter = $('.search').val();
        var date_filter = $('.date_filter').attr('date-filter');
        e.preventDefault();
        if(term_filter == ''){
            var term_filter = '';
        }
        i_loading.show();
        $.ajax({
        type: "POST",
        url: i_infos.ajax_url,
        data: { action: 'soopz_filter_search', status_filter: status_filter,term_filter:term_filter,date_filter:date_filter},
        success: function(response){
        i_loading.hide();
        $("#all_filtered_posts").html(response);
        },
        error: function(error){
        console.log("bad");
        }
        });
        
    });

    $('.date_filter').click(function(e){
        var status_filter = $('.status_filter select').val();
        var term_filter = $('.search').val();
        var date_filter = $('.date_filter').attr('date-filter');
        e.preventDefault();
        var date_class = $('.date_filter i').attr('class');
        if(date_filter == 'desc'){
            var date_filter = 'asc';
        }else{
            var date_filter = 'desc';
        }
        if(date_class == 'ion-chevron-down'){
            $('.date_filter i').removeClass().addClass('ion-chevron-up');
            $('.date_filter').attr('date-filter','asc');
        }else{
            $('.date_filter i').removeClass().addClass('ion-chevron-down');
            $('.date_filter').attr('date-filter','desc');
        }
        i_loading.show();
        $.ajax({
        type: "POST",
        url: i_infos.ajax_url,
        data: { action: 'soopz_filter_date', status_filter: status_filter,term_filter:term_filter,date_filter:date_filter},
        success: function(response){
        i_loading.hide();
        $("#all_filtered_posts").html(response);
        },
        error: function(error){
        console.log("bad");
        }
        });
        
    });

    // Sidebar

    $('body').on('keyup click', '.i_post_editor_input',  function(e) {
        var county = $(this).next();
        var has_text = county.attr('has_text');
        var has_url = county.attr('has_url');
        var has_image = county.attr('has_image');
        var char_count = county.attr('char_count');
        var no_hashtag = county.attr('no_hashtag');
        var has_emoji = county.attr('has_emoji');

        function wp_reset_grey(item){
            item.removeClass().addClass('ion-record grey_icon');
        }

        if(has_text == undefined || has_url == undefined || has_image == undefined){
            wp_reset_grey($('.social_content .content_icons i:nth-child(1),.social_content .content_icons i:nth-child(2),.social_content .content_icons i:nth-child(3)'));
        }



        // Red
        if(has_text == 'true' && has_url == 'false' && has_image == 'false'){
            $('.social_content .content_icons i:nth-child(1)').removeClass().addClass('ion-record red_icon');
            wp_reset_grey($('.social_content .content_icons i:nth-child(2),.social_content .content_icons i:nth-child(3)'));
        }else if(has_url == 'true' && has_text == 'false' && has_image == 'false'){
            $('.social_content .content_icons i:nth-child(1)').removeClass().addClass('ion-record red_icon');
            wp_reset_grey($('.social_content .content_icons i:nth-child(2),.social_content .content_icons i:nth-child(3)'));

        }else if(has_image == 'true' && has_url == 'false' && has_text == 'false'){
            $('.social_content .content_icons i:nth-child(1)').removeClass().addClass('ion-record red_icon');
            wp_reset_grey($('.social_content .content_icons i:nth-child(2),.social_content .content_icons i:nth-child(3)'));
        }



        // Blue
        if(has_text == 'true' && has_url == 'true' && has_image == 'false'){
            $('.social_content .content_icons i:nth-child(1),.social_content .content_icons i:nth-child(2)').removeClass().addClass('ion-record blue_icon');
                wp_reset_grey($('.social_content .content_icons i:nth-child(3)'));
        }else if(has_url == 'true' && has_text == 'false' && has_image == 'true'){
            $('.social_content .content_icons i:nth-child(1),.social_content .content_icons i:nth-child(2)').removeClass().addClass('ion-record blue_icon');
            wp_reset_grey($('.social_content .content_icons i:nth-child(3)'));
        }else if(has_image == 'true' && has_url == 'false' && has_text == 'true'){
            $('.social_content .content_icons i:nth-child(1),.social_content .content_icons i:nth-child(2)').removeClass().addClass('ion-record blue_icon');
            wp_reset_grey($('.social_content .content_icons i:nth-child(3)'));
        }


        // green
        if(has_text == 'true' && has_url == 'true' && has_image == 'true'){
            $('.social_content .content_icons i:nth-child(1),.social_content .content_icons i:nth-child(2),.social_content .content_icons i:nth-child(3)').removeClass().addClass('ion-record green_icon');
        }





        if(has_text == 'true'){
            $('.social_content ul li:nth-child(1) i').removeClass().addClass('ion-checkmark-circled green_icon_borders');
        }else{
            $('.social_content ul li:nth-child(1) i').removeClass().addClass('ion-checkmark-circled grey_icon_borders');
        }

        if(has_url == 'true'){
            $('.social_content ul li:nth-child(3) i').removeClass().addClass('ion-checkmark-circled green_icon_borders');
        }else{
            $('.social_content ul li:nth-child(3) i').removeClass().addClass('ion-checkmark-circled grey_icon_borders');
        }

        if(has_image == 'true'){
            $('.social_content ul li:nth-child(2) i').removeClass().addClass('ion-checkmark-circled green_icon_borders');
        }else{
            $('.social_content ul li:nth-child(2) i').removeClass().addClass('ion-checkmark-circled grey_icon_borders');
        }



        if(char_count == undefined){
            wp_reset_grey($('.social_characters .content_icons i:nth-child(1),.social_characters .content_icons i:nth-child(2),.social_characters .content_icons i:nth-child(3)'));
        }
        if(char_count == '90'){
            $('.social_characters .content_icons i:nth-child(1)').removeClass().addClass('ion-record red_icon');
            wp_reset_grey($('.social_characters .content_icons i:nth-child(2),.social_characters .content_icons i:nth-child(3)'));
        }else if(char_count == '109'){
            $('.social_characters .content_icons i:nth-child(1),.social_characters .content_icons i:nth-child(2)').removeClass().addClass('ion-record blue_icon');
            wp_reset_grey($('.social_characters .content_icons i:nth-child(3)'));
        }else if(char_count == '110'){
            $('.social_characters .content_icons i:nth-child(1),.social_characters .content_icons i:nth-child(2),.social_characters .content_icons i:nth-child(3)').removeClass().addClass('ion-record green_icon');
        }else if(char_count == '115'){
            $('.social_characters .content_icons i:nth-child(1),.social_characters .content_icons i:nth-child(2)').removeClass().addClass('ion-record blue_icon');
            wp_reset_grey($('.social_characters .content_icons i:nth-child(3)'));
        }


        if(no_hashtag == undefined){
            $('.social_hashtags .content_icons i:nth-child(1),.social_hashtags .content_icons i:nth-child(2),.social_hashtags .content_icons i:nth-child(3)').removeClass().addClass('ion-record green_icon');
        }


        if(no_hashtag == 'true'){
            $('.social_hashtags .content_icons i:nth-child(1),.social_hashtags .content_icons i:nth-child(2),.social_hashtags .content_icons i:nth-child(3)').removeClass().addClass('ion-record green_icon');
        }else if(no_hashtag == 'false' && no_hashtag != undefined){
            $('.social_hashtags .content_icons i:nth-child(1),.social_hashtags .content_icons i:nth-child(2)').removeClass().addClass('ion-record blue_icon');
            wp_reset_grey($('.social_hashtags .content_icons i:nth-child(3)'));
        }
        

        if(has_emoji == 'false' || has_emoji == undefined){
            $('.social_emoji .content_icons i:nth-child(1),.social_emoji .content_icons i:nth-child(2)').removeClass().addClass('ion-record blue_icon');
            wp_reset_grey($('.social_emoji .content_icons i:nth-child(3)'));
        }else{
            $('.social_emoji .content_icons i:nth-child(1),.social_emoji .content_icons i:nth-child(2),.social_emoji .content_icons i:nth-child(3)').removeClass().addClass('ion-record green_icon');
        }


    });



    // Points
    $('body').on('keyup', '.i_post_editor_input',  function(e) {
        var county = $(this).next();
        if($(this).val().length !== 0){
            if(county.attr('has_image') != 'true'){
                county.attr('has_image','false');
            }
            if(county.attr('has_url') != 'true'){
                county.attr('has_url','false');
            }

            // Score colors
            if(parseInt(county.text()) >= 50 && parseInt(county.text()) < 69){
                $($(this)).parent().find('.scores i:nth-child(1)').removeClass().addClass('ion-record firestorm');
                $($(this)).parent().find('.scores i:nth-child(2),.scores i:nth-child(3)').removeClass().addClass('ion-record grey_icon');
            }

            if(parseInt(county.text()) >= 70 && parseInt(county.text()) < 89){
                $($(this)).parent().find('.scores i:nth-child(1),.scores i:nth-child(2)').removeClass().addClass('ion-record taser');
                $($(this)).parent().find('.scores i:nth-child(3)').removeClass().addClass('ion-record grey_icon');
            }

            if(parseInt(county.text()) >= 90){
                $($(this)).parent().find('.scores i:nth-child(1),.scores i:nth-child(2),.scores i:nth-child(3)').removeClass().addClass('ion-record hulksmash');
            }


            // Text validation
            if(county.attr('has_text') != 'true'){
                var new_county = parseInt(county.text()) + 5;
                county.text(new_county);
                county.attr('has_text','true');
            }


            // Hashtag validation
            var hash = /(^|\s)([#@][a-z\d-]+)/;
            if($(this).val().match(hash)){
                if(county.attr('no_hashtag') == 'true'){
                    var new_county = parseInt(county.text()) - 15;
                    county.text(new_county);
                    county.attr('no_hashtag','false');
                }
            }else{
                if(county.attr('no_hashtag') != 'true'){
                    var new_county = parseInt(county.text()) + 25;
                    county.text(new_county);
                    county.attr('no_hashtag','true');
                }
            }

            // Emoji validation
            var emoj = /(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff]|[\u0023-\u0039]\ufe0f?\u20e3|\u3299|\u3297|\u303d|\u3030|\u24c2|\ud83c[\udd70-\udd71]|\ud83c[\udd7e-\udd7f]|\ud83c\udd8e|\ud83c[\udd91-\udd9a]|\ud83c[\udde6-\uddff]|[\ud83c[\ude01-\ude02]|\ud83c\ude1a|\ud83c\ude2f|[\ud83c[\ude32-\ude3a]|[\ud83c[\ude50-\ude51]|\u203c|\u2049|[\u25aa-\u25ab]|\u25b6|\u25c0|[\u25fb-\u25fe]|\u00a9|\u00ae|\u2122|\u2139|\ud83c\udc04|[\u2600-\u26FF]|\u2b05|\u2b06|\u2b07|\u2b1b|\u2b1c|\u2b50|\u2b55|\u231a|\u231b|\u2328|\u23cf|[\u23e9-\u23f3]|[\u23f8-\u23fa]|\ud83c\udccf|\u2934|\u2935|[\u2190-\u21ff])/g;
            if($(this).val().match(emoj)){
                if(county.attr('has_emoji') != 'true'){
                    if(county.attr('has_emoji') == 'false'){
                        var new_county = parseInt(county.text()) - 15;
                        county.text(new_county);
                    }
                    var new_county = parseInt(county.text()) + 25;
                    county.text(new_county);
                    county.attr('has_emoji','true');
                }
            }else{
                if(county.attr('has_emoji') != 'false'){
                    if(county.attr('has_emoji') == 'true'){
                        var new_county = parseInt(county.text()) - 25;
                        county.text(new_county);
                    }
                    var new_county = parseInt(county.text()) + 15;
                    county.text(new_county);
                    county.attr('has_emoji','false');
                }
            }


            // URL Validation
             var exp = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/; 
             if($(this).val().match(exp)) {
                if(county.attr('has_url') != 'true'){
                    var new_county = parseInt(county.text()) + 10;
                    county.text(new_county);
                    county.attr('has_url','true');
                }
            }else{
                if(county.attr('has_url') == 'true'){
                    var new_county = parseInt(county.text()) - 10;
                    county.text(new_county);
                    county.attr('has_url','false');
                }
            }

            // Character validation
            // 115
            if($(this).val().length > 115){
                if(county.attr('char_count') == '110'){
                    var new_county = parseInt(county.text()) - 25;
                    county.text(new_county);
                }else if(county.attr('char_count') == '109'){
                    var new_county = parseInt(county.text()) - 15;
                    county.text(new_county);
                }else if(county.attr('char_count') == '90'){
                    var new_county = parseInt(county.text()) - 10;
                    county.text(new_county);
                }

                if(county.attr('char_count') !== '115'){
                    var new_county = parseInt(county.text()) + 10;
                    county.text(new_county);
                    county.attr('char_count','115');
                }
            }

            // 110
            if($(this).val().length >= 110 && $(this).val().length <= 115){
                if(county.attr('char_count') == '115'){
                    var new_county = parseInt(county.text()) - 10;
                    county.text(new_county);
                }else if(county.attr('char_count') == '109'){
                    var new_county = parseInt(county.text()) - 15;
                    county.text(new_county);
                }else if(county.attr('char_count') == '90'){
                    var new_county = parseInt(county.text()) - 10;
                    county.text(new_county);
                }

                if(county.attr('char_count') !== '110'){
                    var new_county = parseInt(county.text()) + 25;
                    county.text(new_county);
                    county.attr('char_count','110');
                }
            }

            // 109
            if($(this).val().length >= 90 && $(this).val().length <= 109){
                if(county.attr('char_count') == '110'){
                    var new_county = parseInt(county.text()) - 25;
                    county.text(new_county);
                }else if(county.attr('char_count') == '115'){
                    var new_county = parseInt(county.text()) - 10;
                    county.text(new_county);
                }else if(county.attr('char_count') == '90'){
                    var new_county = parseInt(county.text()) - 10;
                    county.text(new_county);
                }

                if(county.attr('char_count') !== '109'){
                    var new_county = parseInt(county.text()) + 15;
                    county.text(new_county);
                    county.attr('char_count','109');
                }
            }

            // 90
            if($(this).val().length < 90){
                if(county.attr('char_count') == '110'){
                    var new_county = parseInt(county.text()) - 25;
                    county.text(new_county);
                }else if(county.attr('char_count') == '115'){
                    var new_county = parseInt(county.text()) - 10;
                    county.text(new_county);
                }else if(county.attr('char_count') == '109'){
                    var new_county = parseInt(county.text()) - 15;
                    county.text(new_county);
                }
                if(county.attr('char_count') !== '90'){
                    var new_county = parseInt(county.text()) + 10;
                    county.text(new_county);
                    county.attr('char_count','90');
                }
            }

        }else{
            if(county.attr('has_text') == 'true'){
                var new_county = parseInt(county.text()) - 5;
                county.text(new_county);
                county.attr('has_text','false');
            }
        }
    });




});

function isURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return pattern.test(str);
}



// get image on click function
function get_image_on_click(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            jQuery(input).parent().parent().find('.social_preview_image').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
