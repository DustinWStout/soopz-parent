<?php
/*
Template Name: editor
*/

if ( !is_user_logged_in() ){
    wp_redirect( home_url() );
}

get_header();

$user_id = get_current_user_id();
?>

    <div class="word-count editor_page i_post_input_item_div">
        <h4>Quick start</h4>
    </div>
    <div class="all_soopz_inputs">
        <div class="flex-wrapper soopz-input">
            <span class="select_social_type"><i class="ion-android-person social_active_type social_profile"></i></span>
            <div class="social_types">
                <a href="" title="Facebook"><i class="sw sw-facebook social_facebook"></i></a>
                <a href="" title="Linkedin"><i class="sw sw-linkedin social_linkedin"></i></a>
                <a href="" title="Twitter"><i class="sw sw-twitter social_twitter"></i></a>
                <a href="" title="Instagram"><i class="sw sw-instagram social_instagram"></i></a>
                <a href="" title="Google-plus"><i class="sw sw-google-plus social_google-plus"></i></a>
                <a href="" title="Pinterest"><i class="sw sw-pinterest social_pinterest"></i></a>
            </div>
                <textarea required name="content[]" class="autoExpand2 flex-item form-control i_post_editor_input" type="text" placeholder="Type your social message..."></textarea>
                <span style="display:none;" class="county">0</span>
                <div class="image_uploader">
                    <i class="ion-image"></i>
                </div>
                <div class="image_uploader2">
                    <label for="" class="custom-file-upload">
                        <i class="ion-upload"></i>
                    </label>
                    <input class="social_image input_social_image" name="social_image[]" type="file"/>
                </div>
                <div class="image_uploader3">
                    <i class="ion-close-circled"></i>
                    <div>
                    <img class="social_preview_image" src="">
                    </div>
                </div>
                <div class="popup-content-image">
                    <img class="social_preview_image" src="">
                </div>
                <div class="counters flex-item">
                    <ul class="counters">
                        <li class="char-count"><span>Characters </span><span class="chars item_circle">0</span></li>
                        <li class="word-count"><span>Words</span><span class="words item_circle">0</span></li>
                        <li class="par-count"><span>Paragraphs</span><span class="paras item_circle">0</span></li>
                        <li class="score"><span>Score</span>
                            <span class="scores item_circle">
                                <i class="ion-record grey_icon"></i>
                                <i class="ion-record grey_icon"></i>
                                <i class="ion-record grey_icon"></i>
                            </span>
                        </li>
                        <li class="delete_field"><i class="ion ion-minus-circled i_remove_filed" aria-hidden="true" title="Remove this field"></i> </li>
                    </ul>
                </div>
        </div>
    </div>
<div class="flex-wrapper new_textarea ion-ios-plus-outline">
    <span class="flex-item new-input">New input</span>
    <span class="empty-flex-item"></span>
</div>
<div class="text_area_items">
    <div id="global-count">
        <span class="flex-item">Characters</span><span class="chars item_circle">0</span>
        <span class="flex-item">Words</span><span class="words item_circle">0</span>
        <span class="flex-item">Paragraphs</span><span class="paras item_circle">0</span>
    </div>
    <div class="textare_buttons">
        <span id="btn-open" class="button" title="Share">Share</span>
        <span id="btn-save" class="button" title="Save">Save</span>
        <input type="hidden" value="" id="i_post_id">
        <span id="copy_to_clipboard" class="button" title="Copy All">Copy All</span>
        <span id="btn_update_post" class="button" style="display:none;" title="Update">Update</span>
        <span id="btn_cancel_action" class="button" style="display:none;" title="Cancel - back to New Post">Cancel</span>
        <textarea id="copy_to_clipboard_area" class="hidden"></textarea>
        <div class="copied i_post_actions_msg"></div>
    </div>
    <p class="post_success_message"><?php _e('Your post has been successfully saved!'); ?></p>
    <div id="i_loading" class="ajax_loader_image"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif"></div>
</div>
<div class="all_posts">
    <div class="flex-wrapper flex-head">
        <h4 class="flex-item">Posts</h4><input type="search" class="flex-item" placeholder="Search posts..." />
    </div>
    <div id="i_posts_list" class="post_row">
        <?php
        $args = array(
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'author' => $user_id
        );
        $posts_array = get_posts($args);
        foreach ($posts_array as $value) {
            $post_id = $value->ID;
            $post_title = $value->post_title;
            $post_content = $value->post_content;
            ?>

        <div id="i_post_item_div_<?php echo $post_id;?>" class="post_item_row">
            <a data-post-id="<?php echo $post_id;?>" class="i_post_edit" href="">
                <span id="post_title_<?php echo $post_id;?>" class="post_text"><?php echo $post_title; ?></span>
                <i class="ion ion-compose i_post_edit" aria-hidden="true" data-post-id="<?php echo $post_id;?>"></i>
            </a>
            <i class="ion ion-close-circled wi-remove i_post_remove" data-post-id="<?php echo $post_id;?>" data-post-title="<?php echo $post_title; ?>" title="Remove this post" aria-hidden="true"></i>
        </div>
        <?php
        }
        ?>
    </div>
</div>

<div id="popup-content">
    <div class="popup_content"></div>
    <p class="post_success_message_for_popup">Your post has been successfully saved!</p>
    <div id="i_loading_for_popup" class="ajax_loader_image"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif"></div>
    <div class="popup_footer">
        <div class="popup_footer_text">
            <span>Highlight the text and press CMD+C to copy all text.</span>
        </div>
        <div class="popup_footer_buttons">
            <span class="button popup-button">Share</span>
            <span id="popup-save-btn" class="button save-button">Save</span>
        </div>
    </div>
</div>
<div id="popup-content-share">
    <div class="popup_content_share">
        <div id="social-buttons" class="">
            <?php
            if (function_exists('social_warfare')):
                social_warfare();
            endif;
            ?>
        </div>
        <div class="sharing_text">
            <p>Your selected text will be added to the network share of your choosing. For multiple inputs, use the Copy All option.</p>
            <p>Sharing courtesty of <strong><a href="#">Social Warfare</a></strong> by <strong><a href="https://warfareplugins.com">Warfare Plugins</a></strong>.</p>
        </div>
    </div>

</div>




<?php get_footer(); ?>
