<?php
/*
Template Name: post
*/

if ( !is_user_logged_in() ){
    wp_redirect( home_url() );
}

$user_id = get_current_user_id();

get_header();
?>


<div class="all_posts">
    <div class="twelve columns">
        <h4>Posts</h4>
        <form id="filter_form" action="" method='get'>
            <label class='select_filter_label status_filter' for="status">
                <select name="status" class='select_filter'>
                    <option data-display="STATUS" value=""></option>
                    <option value="publish">published</option>
                    <option value="draft">draft</option>
                </select>
            </label>

            <label class='select_filter_label types_filter' for="types">
                <select name="types" class='select_filter'>
                    <option data-display="TYPES" value=""></option>
                    <option value="facebook">facebook</option>
                    <option value="linkedin">linkedin</option>
                    <option value="twitter">twitter</option>
                    <option value="instagram">instagram</option>
                    <option value="googleplus">google+</option>
                    <option value="pinterest">pinterest</option>
                </select>
            </label>

            <label class='select_filter_label scores_filter' for="scores">
                <select name="scores" class='select_filter'>
                    <option data-display="SCORES" value=""></option>
                    <option value="3"></option>
                    <option value="2"></option>
                    <option value="1"></option>
                    <option value="0"></option>
                </select>
            </label>
            <input type="text" name="s" required class="search" placeholder="Search">
            <button type="submit" class="search_icon search_filter"><i class="ion-search" data-pack="default" data-tags="magnifying glass"></i></button>
            <!--<button type="submit" class="search_icon">&#xf002;</button>-->
            <!--<i class="ion-search" data-pack="default" data-tags="magnifying glass"></i>-->
        </form>
    </div>
    
    <div id="i_posts_list">
        <div class="nine columns post_row" style="margin-left: 0;">
            <h5>Content</h5>
        </div>
        <div class="three columns">
            <div class="score_div">
                <h5>Score</h5>
            </div>
            <div class="date_div">
                <h5 date-filter="desc" class="date_filter">Date <i class="ion-chevron-down"></i></h5>
            </div>
        </div>
            <div id="i_loading" class="ajax_loader_image"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/ajax-loader.gif"></div>
            <div id="all_filtered_posts">
<?php
$args = array(
    'post_type' => 'post',
    'post_status' => 'any',
    'posts_per_page' => -1,
    'author' => $user_id,
    'order'=>'desc',
                    );
$query = new WP_Query ( $args );
while ($query -> have_posts()): $query -> the_post();
?>
                <div class="one_post">
                    <div class="nine columns post_row" style="margin-left: 0;">
                        
                        <div id="i_post_item_div_<?php the_ID(); ?>" class="post_item_row">
                            <a target="_blanck" data-post-id="<?php the_ID(); ?>" class="i_post_edit" href="<?php the_permalink(); ?>">
                                <span id="post_title_<?php the_ID(); ?>" class="post_text"><?php the_title(); ?></span>
                                <i class="ion ion-compose i_post_edit" aria-hidden="true" data-post-id="<?php the_ID(); ?>"></i>
                            </a>

                        </div>
                    </div>
            
                    <div class="three columns">
                        <div class="post_score">
                            <i class='ion-record green_icon'></i><i class='ion-record green_icon'></i><i class='ion-record green_icon'></i>
                        </div>
                        <div class="post_date">
                            <span><?php the_time('m/d/y') ?></span>
                          <i class="ion ion-close-circled wi-remove post_remove" data-post-id="<?php the_ID(); ?>" data-post-title="<?php the_title(); ?>" title="Remove this post" aria-hidden="true"></i>
                        </div>

                    </div>
                </div>
<?php endwhile; wp_reset_query(); ?>
        </div>
    </div>

    
</div>
<?php get_footer(); ?>
